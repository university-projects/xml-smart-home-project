var doc = document.implementation.createDocument(null, null);
var rootElement = doc.createElement("smarthome");
doc.appendChild(rootElement);

var roomsElement = doc.createElement("rooms");
rootElement.appendChild(roomsElement);

var roomElement1 = doc.createElement("room");
roomElement1.setAttribute("id", "1");
roomsElement.appendChild(roomElement1);

var doorsElement =  doc.createElement("doors");
roomElement1.appendChild(doorsElement);

var doorElement1 = doc.createElement("door");
doorElement1.setAttribute("id", "1");
doorElement1.setAttribute("state", "opened");
doorsElement.appendChild(doorElement1);

var windowsElement =  doc.createElement("windows");
roomElement1.appendChild(windowsElement);

var windowElement1 = doc.createElement("window");
windowElement1.setAttribute("id", "1");
windowElement1.setAttribute("state", "closed");
windowsElement.appendChild(windowElement1);

var devicesElement =  doc.createElement("devices");
roomElement1.appendChild(devicesElement);

var tvElement = doc.createElement("device");
tvElement.setAttribute("id", "1");
tvElement.setAttribute("type", "tv");
tvElement.setAttribute("state", "off");
devicesElement.appendChild(tvElement);

var lightsElement =  doc.createElement("lights");
roomElement1.appendChild(lightsElement);

var redLightsElement = doc.createElement("light");
redLightsElement.setAttribute("id", "1");
redLightsElement.setAttribute("type", "colored");
redLightsElement.setAttribute("state", "on");
lightsElement.appendChild(redLightsElement);

var intensityElement = doc.createElement("intensity");
intensityElement.innerHTML = "100";
redLightsElement.appendChild(intensityElement);

var colorElement = doc.createElement("color");
colorElement.innerHTML = "red";
redLightsElement.appendChild(colorElement);

var temperatureElement = doc.createElement("temperature");
roomElement1.appendChild(temperatureElement);

var timestampElement1 = doc.createElement("timestamp");
timestampElement1.innerHTML = "1484256371";
temperatureElement.appendChild(timestampElement1);

var valueElement1 = doc.createElement("value");
valueElement1.innerHTML = "24.4";
temperatureElement.appendChild(valueElement1);

var unitsElement1 = doc.createElement("units");
unitsElement1.innerHTML = "C";
temperatureElement.appendChild(unitsElement1);

var humidityElement = doc.createElement("humidity");
roomElement1.appendChild(humidityElement);

var timestampElement2 = doc.createElement("timestamp");
timestampElement2.innerHTML = "1484256371";
humidityElement.appendChild(timestampElement2);

var valueElement2 = doc.createElement("value");
valueElement2.innerHTML = "30.1";
humidityElement.appendChild(valueElement2);

var unitsElement2 = doc.createElement("units");
unitsElement2.innerHTML = "%";
humidityElement.appendChild(unitsElement2);

var camerasElement = doc.createElement("cameras");
rootElement.appendChild(camerasElement);

var cameraElement1 = doc.createElement("camera");
cameraElement1.setAttribute("id", "1");
cameraElement1.setAttribute("state", "on");
camerasElement.appendChild(cameraElement1);

var positionElement = doc.createElement("position");
positionElement.innerHTML = "the front door";
cameraElement1.appendChild(positionElement);

var qualityElement = doc.createElement("quality");
qualityElement.innerHTML = "HD";
cameraElement1.appendChild(qualityElement);

var snapshotElement = doc.createElement("snapshot");
snapshotElement.innerHTML = "http://snapshot.com/1231das12";
cameraElement1.appendChild(snapshotElement);

var personElem1 = doc.createElement("person");
personElem1.setAttribute("first-name", "eric");
personElem1.setAttribute("middle-initial", "h");
personElem1.setAttribute("last-name", "jung");

var addressElem1 = doc.createElement("address");
addressElem1.setAttribute("street", "321 south st");
addressElem1.setAttribute("city", "denver");
addressElem1.setAttribute("state", "co");
addressElem1.setAttribute("country", "usa");
personElem1.appendChild(addressElem1);

var xml = new XMLSerializer().serializeToString(doc);
console.log(xml);
