<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

	<xsl:output method="xml" indent="yes"/>
	<xsl:template match="/">
		<belongings>
			<rooms>
        <xsl:value-of select="count(smarthome/rooms/room)"/>
      </rooms>
      <doors>
        <xsl:value-of select="count(smarthome/rooms/room/doors/door)"/>
      </doors>
      <windows>
        <xsl:value-of select="count(smarthome/rooms/room/windows/window)"/>
      </windows>
      <devices>
        <xsl:value-of select="count(smarthome/rooms/room/devices/device)"/>
      </devices>
      <lights>
        <xsl:value-of select="count(smarthome/rooms/room/lights/light)"/>
      </lights>
      <cameras>
        <xsl:value-of select="count(smarthome/cameras/camera)"/>
      </cameras>
    </belongings>
	</xsl:template>

</xsl:stylesheet>
