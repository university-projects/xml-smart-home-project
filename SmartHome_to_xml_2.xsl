<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

	<xsl:output method="xml" indent="yes"/>
	<xsl:template match="/">
		<status>
      <doors>
        <total>
          <xsl:value-of select="count(smarthome/rooms/room/doors/door)"/>
        </total>
        <opened>
          <xsl:value-of select="count(smarthome/rooms/room/doors/door[@state='opened'])"/>
        </opened>
        <closed>
          <xsl:value-of select="count(smarthome/rooms/room/doors/door[@state='closed'])"/>
        </closed>
      </doors>
      <windows>
        <total>
          <xsl:value-of select="count(smarthome/rooms/room/windows/window)"/>
        </total>
        <opened>
          <xsl:value-of select="count(smarthome/rooms/room/windows/window[@state='opened'])"/>
        </opened>
        <closed>
          <xsl:value-of select="count(smarthome/rooms/room/windows/window[@state='closed'])"/>
        </closed>
      </windows>
      <devices>
        <total>
          <xsl:value-of select="count(smarthome/rooms/room/devices/device)"/>
        </total>
        <on>
          <xsl:value-of select="count(smarthome/rooms/room/devices/device[@state='on'])"/>
        </on>
        <off>
          <xsl:value-of select="count(smarthome/rooms/room/devices/device[@state='off'])"/>
        </off>
      </devices>
      <lights>
        <total>
          <xsl:value-of select="count(smarthome/rooms/room/lights/light)"/>
        </total>
        <on>
          <xsl:value-of select="count(smarthome/rooms/room/lights/light[@state='on'])"/>
        </on>
        <off>
          <xsl:value-of select="count(smarthome/rooms/room/lights/light[@state='off'])"/>
        </off>
      </lights>
      <cameras>
        <total>
          <xsl:value-of select="count(smarthome/cameras/camera)"/>
        </total>
        <on>
          <xsl:value-of select="count(smarthome/cameras/camera[@state='on'])"/>
        </on>
        <off>
          <xsl:value-of select="count(smarthome//cameras/camera[@state='off'])"/>
        </off>
      </cameras>
    </status>
	</xsl:template>

</xsl:stylesheet>
