<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/">
		<html>
			<head>
			</head>
			<body>
				<h1>SmartHome</h1>
        <h3>There are <xsl:value-of select="count(smarthome/rooms/room)"/> rooms.</h3>
        <h3>There are <xsl:value-of select="count(smarthome/rooms/room/doors/door)"/> doors.</h3>
        <h3>There are <xsl:value-of select="count(smarthome/rooms/room/windows/window)"/> windows.</h3>
        <h3>There are <xsl:value-of select="count(smarthome/rooms/room/devices/device)"/> devices.</h3>
        <h3>There are <xsl:value-of select="count(smarthome/rooms/room/lights/light)"/> lights.</h3>
        <h3>There are <xsl:value-of select="count(smarthome/cameras/camera)"/> cameras.</h3>

			</body>
		</html>
	</xsl:template>

</xsl:stylesheet>
