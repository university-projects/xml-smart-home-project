<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/">
		<html>
			<head>
				<style>
					h2 {padding-left: 20px;}
					h3 {padding-left: 40px;}
				</style>
			</head>
			<body>
				<h1>SmartHome</h1>

				<xsl:for-each select="smarthome/rooms/room">
					<h2>room <xsl:value-of select="@id" /></h2>
					<xsl:for-each select="doors/door">
						<h3>door <xsl:value-of select="@id" /> | Status: <xsl:value-of select="@state" /></h3>
					</xsl:for-each>
					<xsl:for-each select="windows/window">
						<h3>window <xsl:value-of select="@id" /> | Status: <xsl:value-of select="@state" /></h3>
					</xsl:for-each>
					<xsl:for-each select="devices/device">
						<h3>device <xsl:value-of select="@id" /> | Type: <xsl:value-of select="@type" />  | State: <xsl:value-of select="@state" /></h3>
					</xsl:for-each>
					<xsl:for-each select="lights/light">
						<h3>lights <xsl:value-of select="@id" /> | Type: <xsl:value-of select="@type" />  | State: <xsl:value-of select="@state" /> | Intensity: <xsl:value-of select="intensity" /> | Color: <xsl:value-of select="color" /></h3>
					</xsl:for-each>
					<h3>The temperature is <xsl:value-of select="temperature/value" /> <xsl:value-of select="temperature/units" />, measured at <xsl:value-of select="temperature/timestamp" /></h3>
					<h3>The humidity is <xsl:value-of select="humidity/value" /> <xsl:value-of select="humidity/units" />, measured at <xsl:value-of select="humidity/timestamp" /></h3>
				</xsl:for-each>
				<xsl:for-each select="smarthome/cameras/camera">
					<h2>camera <xsl:value-of select="@id" /> | State: <xsl:value-of select="@state" /> | Position: <xsl:value-of select="position" /> | Quality: <xsl:value-of select="quality" /> | Snapshot: <xsl:value-of select="snapshot" /></h2>
				</xsl:for-each>
			</body>
		</html>
	</xsl:template>

</xsl:stylesheet>
