<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" indent="yes"/>
	<xsl:template match="/">
				<xsl:for-each select="smarthome/rooms/room">
In room <xsl:value-of select="@id" />:<xsl:for-each select="doors/door"> door <xsl:value-of select="@id" /> is <xsl:value-of select="@state" />,</xsl:for-each><xsl:for-each select="windows/window">window <xsl:value-of select="@id" /> is <xsl:value-of select="@state" />,</xsl:for-each><xsl:for-each select="devices/device"><xsl:value-of select="@type" /> (<xsl:value-of select="@id" />) is <xsl:value-of select="@state" />,</xsl:for-each><xsl:for-each select="lights/light"><xsl:value-of select="@type" /> lights (<xsl:value-of select="@id" />) are <xsl:value-of select="@state" /> at <xsl:value-of select="intensity" /> %, glowing in <xsl:value-of select="color" /> color,</xsl:for-each>the temperature is <xsl:value-of select="temperature/value" /> <xsl:value-of select="temperature/units" />,the humidity is <xsl:value-of select="humidity/value" /> <xsl:value-of select="humidity/units" />.
				</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
