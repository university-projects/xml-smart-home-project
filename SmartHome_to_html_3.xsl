<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/">
		<html>
			<head>
			</head>
			<body>
				<h1>Cameras</h1>

				<xsl:for-each select="smarthome/cameras/camera">
					<p>
						Camera <xsl:value-of select="@id" /> is <xsl:value-of select="@state" />.
            It is looking at <xsl:value-of select="position" />,
            capturing at <xsl:value-of select="quality" /> quality.
            A snapshot can be previewed at <xsl:value-of select='snapshot' />
					</p>
				</xsl:for-each>
			</body>
		</html>
	</xsl:template>

</xsl:stylesheet>
